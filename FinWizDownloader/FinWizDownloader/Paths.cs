namespace FinWizDownloader
{
    class Paths
    {
        private const string Directory = "c:\\aa";

        public static string GetAllCsvFile()
        {
            return $"{Directory}\\all.csv";
        }
        public static string GetSectorsFile()
        {
            return $"{Directory}\\all-sectors.csv";
        }

        public static string GetHtmlFile(int strana)
        {
            return $"{Directory}\\finviz-{strana:D2}.html";
        }

        public static string GetCsvFile(int strana)
        {
            return $"{Directory}\\csv-{strana:D2}.csv";
        }
    }
}