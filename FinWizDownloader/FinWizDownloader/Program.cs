﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NLog;

namespace FinWizDownloader
{
    class Program
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        private const string Url = "http://finviz.com/screener.ashx?v=111&f=cap_smallover,geo_usa,sh_avgvol_o200,sh_price_o10&r=";
        private const int PocetStranVeVypisu = 90;
        private const int PocetAkciiNaStrance = 20;

        static void Main()
        {
            // faze 1 - stahovani
            //Phase1_GetAllHtmls();

            // faze 2 - prevod do csv
            //Phase2_ConvertHtmlToCsv();

            // faze 3 - slouceni csv
            //Phase3_ToOneCsv();

            // faze 4 - seznam sektoru
            Phase4_ExtractSectors();
        }

        static IEnumerable<int> GetAllSides()
        {
            //return new List<int> {1};
            return Enumerable.Range(1, PocetStranVeVypisu).ToList();
        }

        static void Phase1_GetAllHtmls()
        {
            var pocet = 1;
            var strana = 1;

            while (pocet < PocetStranVeVypisu * PocetAkciiNaStrance)
            {
                var myUrl = $"{Url}{pocet}";
                var myFile = Paths.GetHtmlFile(strana);

                GetHtml(myUrl, myFile);
                strana++;
                pocet += PocetAkciiNaStrance;

                Log.Debug("Sleeping 5 seconds");
                Thread.Sleep(5 * 1000);
            }
        }

        static void Phase2_ConvertHtmlToCsv()
        {
            foreach (var side in GetAllSides())
            {
                var filename = Paths.GetHtmlFile(side);
                var text = File.ReadAllText(filename);

                var tickers = GetTickers(text);
                var nameAndSector = GetNameAndSector(text);

                if (tickers.Count() != nameAndSector.Count()) throw new Exception($"tickers.Count() != nameAndSector.Count() {tickers.Count()} != {nameAndSector.Count()} side {side}");

                var sb = new StringBuilder();
                for (int i = 0; i < tickers.Count(); i++)
                {
                    var item = $"{tickers.ToArray()[i]};{nameAndSector.ToArray()[i]}";
                    sb.AppendLine(item);
                }
                Log.Debug($"Zapisuji soubor {Paths.GetCsvFile(side)}");
                File.WriteAllText(Paths.GetCsvFile(side), sb.ToString());
            }
        }

        static void Phase3_ToOneCsv()
        {
            var sb = new StringBuilder();
            foreach (var side in GetAllSides())
            {
                var file = Paths.GetCsvFile(side);
                var lines = File.ReadAllLines(file);
                lines.ToList().ForEach(line => sb.AppendLine(line));
            }
            var csv = Paths.GetAllCsvFile();
            Log.Debug($"Zapisuji soubor {csv}");
            File.WriteAllText(csv, sb.ToString());
        }

        static void Phase4_ExtractSectors()
        {
            var csv = Paths.GetAllCsvFile();
            var lines = File.ReadAllLines(csv);

            var slovnik = new Dictionary<string, string>();

            foreach (var line in lines)
            {
                var pole = line.Split(';');
                var ticker = pole[0];
                var sector = pole[3];

                if (!slovnik.ContainsKey(sector)) slovnik.Add(sector, "");
                slovnik[sector] = $"{slovnik[sector]};{ticker}";
            }
            // generovani 
            var sb = new StringBuilder();

            foreach (var key in slovnik.Keys) sb.AppendLine($"{key}{slovnik[key]}");
            var file = Paths.GetSectorsFile();
            File.WriteAllText(file, sb.ToString());
        }

        private static IEnumerable<string> GetTickers(string text)
        {
            var result = new List<string>();

            var matches = Regex.Matches(text, "quote.ashx.t=([^&]+)");
            foreach (Match match in matches) foreach (Group gr in match.Groups)
            {
                var val = gr.Value;
                if (val.Contains("?")) continue;
                if (result.Contains(val)) continue;
                Log.Debug($"* adding symbol {gr.Value}");
                result.Add(val);
            }
            return result;
        }

        private static IEnumerable<string> GetNameAndSector(string text)
        {
            var result = new List<string>();

            var matches = Regex.Matches(text, "class=.screener-link..([^<]+)..a");

            var first = 0;
            var name = "";
            var sector = "";
            foreach (Match match in matches)
            foreach (Group gr in match.Groups)
            {
                var val = gr.Value;
                if (val.Contains("screener-link")) continue;
                if (val.Contains(">")) continue;

                    if (first == 0)
                {
                    first = int.Parse(val);
                    continue;
                }
                if (name == "")
                {
                    name = val;
                    continue;
                }
                if (sector == "")
                {
                    sector = val;
                    continue;
                }

                var iint = ParseInt(val);
                if (iint <= 0) continue;

                if (first + 1 != iint)
                {
                    Log.Debug($"{first} + 1 != {iint}");
                        continue;
                }

                Log.Debug($"* adding name - {first};{name};{sector}");
                result.Add($"{first};{name};{sector}");

                first = iint;
                name = "";
                sector = "";
            }
            Log.Debug($"* adding name - {first};{name};{sector}");
            result.Add($"{first};{name};{sector}");

            return result;
        }

        static int ParseInt(string str)
        {
            try
            {
                return int.Parse(str);
            }
            catch (Exception)
            {
                return -1;
            }
        }

        static void ParseTickers(string filename, string cvs)
        {
            var html = File.ReadAllText(filename);
            var str = "screener-link-primary";
        }

        static void GetHtml(string url, string filename)
        {
            Log.Debug($"Stahuju url {url} do souboru {filename}");
            var client = new WebClient();
            client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
            client.DownloadFile(url, filename);
        }
    }
}
